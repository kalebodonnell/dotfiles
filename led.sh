#!/bin/bash
echo "═══LED═══"
echo "I hope you're ready because this is the last one"
echo "[LED] INSTALLING YAY"
sudo pacman --needed -S base-devel git go
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd
rm -rf yay
echo "[LED] INSTALLING STOW"
yay -S stow
echo "[LED] STOW DESTROYS CP WITH FACTS AND LOGIC"
ls -d */
stow */

echo "[LED] INSTALLING PROGRAMS"
yay -S --needed xorg compton feh inkscape qutebrowser xorg-xinit fish

echo "[LED] COMPILING SUCKLESS AND LINKING THEM TO ~/.bin"
cd ~/sucklessSource
cd dmenu
make
ln -s dmenu ~/.bin
ln -s dmenu_run ~/.bin
ln -s stest ~/.bin
ln -s dmenu_path ~/.bin
cd ../dwm
make
ln -s dwm ~/.bin
cd ../st
make
ln -s st ~/.bin
echo "═══END LED═══"
