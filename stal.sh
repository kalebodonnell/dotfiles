#!/bin/bash

echo "═══STAL═══"
user=$1
pass=$2
echo "[TALL] ADDING GROUP '$user'"
groupadd $user > /dev/null
echo "[TALL] ADDING USER '$user'"
useradd -g $user -m $user > /dev/null
echo "[TALL] SETTING THE PASSWORD FOR '$user'"
passwd $user > /dev/null << EOF
$pass
$pass
EOF
echo "[TALL] MAKING $user A SUDOER"
echo "$user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "═══END STALL═══"
