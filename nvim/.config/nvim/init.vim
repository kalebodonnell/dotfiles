call plug#begin()

Plug 'vim-scripts/L9'
Plug 'fatih/vim-go'
Plug 'majutsushi/tagbar'
Plug 'fatih/molokai'
Plug 'Valloric/YouCompleteMe'
Plug 'nsf/gocode', {'rtp': 'vim/'}
Plug 'tpope/vim-fugitive'
Plug 'SirVer/ultisnips'
Plug 'vim-scripts/FuzzyFinder'
Plug 'tpope/vim-surround'
Plug 'mattn/emmet-vim'

call plug#end()

filetype plugin indent on
"Ok. Done.

let g:netrw_banner = 0
let g:netrw_browse_split = 4

let g:molokai_original=1
set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0
let g:go_fmt_command = "goimports"
set tabstop=2
set shiftwidth=2
set autoindent
set ignorecase
set smartcase
set relativenumber number
set ruler
set autowrite autowriteall
syntax on
set backspace=indent,eol,start
set ic
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.

"if exists ("g:did_load_filetypes")
" filetype off
" filetype plugin indent off
"endif
"set runtimepath+=/usr/local/go/misc/vim
"filetype plugin indent on

"Colors
highlight Pmenu ctermfg=8 ctermbg=7
highlight PmenuSel ctermfg=7 
highlight PmenuSbar ctermfg=8 ctermbg=7
highlight PmenuThumb ctermfg=7 ctermfg=7


highlight SpecialKey ctermfg=6
highlight NonText ctermfg=6

highlight Directory ctermfg=6
highlight ErrorMsg ctermfg=1 ctermbg=0
highlight Search ctermfg=7 ctermbg=3
highlight MoreMsg ctermfg=6

highlight LineNr ctermfg=6 ctermbg=0
highlight CursorLineNr ctermfg=3 ctermbg=0

highlight Question ctermfg=6
highlight StatusLine cterm=none ctermfg=6 ctermbg=0
highlight StatusLineNC ctermfg=3 
highlight VertSplit ctermfg=8 ctermbg=7

highlight Title cterm=Bold
highlight Visual ctermbg=7
highlight WarningMsg ctermfg=3

highlight WildMenu ctermfg=7 ctermbg=3
highlight Folded ctermfg=6 ctermfg=7
highlight FoldColumn ctermfg=6 ctermbg=7

highlight DiffAdd ctermfg=7 ctermbg=4
highlight DiffChange ctermfg=7 ctermbg=5
highlight DiffDelete ctermfg=7 ctermbg=1
highlight DiffText ctermfg=7 ctermbg=6

highlight SignColumn ctermfg=6 ctermbg=7
highlight Conceal ctermfg=6 ctermbg=7

highlight SpellBad ctermbg=1
highlight SpellCap ctermbg=6
highlight SpellRare ctermbg=6
highlight SpellLocal ctermbg=6

highlight TabLine ctermfg=8 ctermbg=7


highlight CursorColumn  ctermfg=7
highlight CursorLine  ctermfg=7

highlight ColorColumn  ctermfg=7

highlight QuickFixLine ctermfg=4 
highlight MatchParen ctermfg=2 ctermbg=0

highlight Delimiter  ctermfg=6
highlight Identifier ctermfg=4
highlight Type ctermfg=4
highlight Error ctermfg=1 ctermbg=0
highlight Comment ctermfg=2
highlight Constant ctermfg=5
highlight Special ctermfg=4
highlight Statment ctermfg=3
highlight PreProc ctermfg=3
highlight Underlined cterm=underline
highlight Ignore ctermfg=17
highlight Todo ctermfg=7 ctermbg=2


" Open file at a position where it was last left.
au BufWrite *.ms silent !groff -ms % -T pdf> /tmp/groff.pdf
au BufRead *.ms silent !bash -c 'zathura /tmp/groff.pdf &'
set so=16

map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
map <M-Tab> <C-W>w

let g:user_emmet_expandabbr_key='<C-Space>'
