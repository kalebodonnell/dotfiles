function fish_prompt
	echo -n "[ "
	set_color cyan
	echo -n (whoami)
	set_color white
	echo -n "@"
	set_color cyan
	echo -n (hostname)
	set_color yellow
	echo -n ' '
	echo -n (prompt_pwd)
	set_color white
	echo -n ' ]$ '
end
