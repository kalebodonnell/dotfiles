alias clip="xclip -in -selection clipboard"
alias in_sec="xclip -in -selection secondary"
alias out_sec="xclip -out -selection secondary"

abbr --add --global nvom nvim
abbr --add --global nv nvim
