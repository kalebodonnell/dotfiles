#!/bin/bash
echo "UPDATING CLOCK"
timedatectl set-ntp true
echo "PARTITIONING"
lsblk
read -p "drive: " drv
fdisk /dev/$drv << EOF
g
n


+100M
t
1
n


p
w
EOF
read
mkfs.ext4 /dev/$(echo $drv)2
mkfs.fat /dev/$(echo $drv)1
read
mount /dev/$(echo $drv)2 /mnt
mkdir /mnt/boot
mount /dev/$(echo $drv)1 /mnt/boot
read
pacstrap /mnt base networkmanager
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt << EOF
ln -sf /usr/share/zoneinfo/Pacific/Auckland /etc/localtime
hwclock --systohc
echo "en_NZ.UTF-8 UTF-8\
en_US.UTF-8 UTF-8" > /etc/locale.gen
echo "LANG=en_NZ.UTF-8"
read -p "hostname: " host
echo "\$host" > /etc/hostname
echo "127.0.0.1 \$host" >> /etc/hosts
curl https://raw.githubusercontent.com/LukeSmithxyz/etc/master/ips >> /etc/hosts
bootctl install --path=/boot
cat > /boot/loader/entries/arch.conf << EOF2
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=PARTUUID=89e62d92-a4d8-f44a-9b45-54ec4141e504 rw
EOF2
EOF
echo "Done!"
