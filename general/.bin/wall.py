#!/bin/python

import time
import os
import datetime 
import math

def colrange(start,end,num,now):
    r = (start[0]-end[0])
    g = (start[1]-end[1])
    b = (start[2]-end[2])
    rstep = r/num
    gstep = g/num
    bstep = b/num
    mid = num / 2
    q1 = mid/2
    q2 = mid + (mid / 2)
    if now <= q1:
        col1 = [
                math.floor(
                    start[0]
                    -rstep
                    *(now*2)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(now)*2
                ),
                math.floor(
                    start[2]
                    -bstep
                    *(now*2)
                )
                ]
        col2 = [
                math.floor(
                    start[0]
                    -rstep
                    *(now)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(now)
                ), 
                math.floor(
                    start[2]
                    -bstep
                    *(now)
                )
                ]


    elif now > q1 and now <= mid:
        col1 = [
                math.floor(
                    start[0]
                    -rstep
                    *(mid)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(mid)
                ),
                math.floor(
                    start[2]
                    -bstep
                    *(mid)
                )
                ]
        col2 = [
                math.floor(
                    start[0]
                    -rstep
                    *(now)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(now)
                ), 
                math.floor(
                    start[2]
                    -bstep
                    *(now)
                )
                ]
    elif now > mid and now < q2:
        col1 = [
                math.floor(
                    start[0]
                    -rstep
                    *(now)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(now)
                ),
                math.floor(
                    start[2]
                    -bstep
                    *(now)
                )
                ]
        
        col2 = [
                math.floor(
                    start[0]
                    -rstep
                    *(((now-mid) * 2)+mid)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(((now-mid) * 2)+mid)
                ), 
                math.floor(
                    start[2]
                    -bstep
                    *(((now-mid) * 2)+mid)
                )
                ]
    elif now >= q2:
        col1 = [
                math.floor(
                    start[0]
                    -rstep
                    *(now)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(now)
                ),
                math.floor(
                    start[2]
                    -bstep
                    *(now)
                )
                ]
        col2 = [
                math.floor(
                    start[0]
                    -rstep
                    *(num)
                ),
                math.floor(
                    start[1]
                    -gstep
                    *(num)
                ), 
                math.floor(
                    start[2]
                    -bstep
                    *(num)
                )
                ]
    for col in col1:
        if col < 0:
            col = 0
    for col in col2:
        if col < 0:
            col = 0
    return [col1, col2]

def getcolor(now):
    startcol = [0,   128, 255]
    endcol   = [64, 0,   0]
    starttime=datetime.datetime(now.year, now.month, now.day, 6, 0)
    endtime=datetime.datetime(now.year, now.month, now.day, 20, 0)
    difftime = (starttime - now).total_seconds()
    colnow = colrange(
            startcol,
            endcol,
            (starttime-endtime).total_seconds(),
            difftime)
    return colnow

def sbg(col1, col2):
    svg = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg width="1366" height="768">
  <defs>
    <linearGradient id="grad">
    <stop offset="0%" style="stop-color: rgb({}, {}, {})"/>
    <stop offset="100%" style="stop-color: rgb({}, {}, {})"/>
    </linearGradient>
  </defs>
  <rect
     y="0"
     x="0"
     height="768"
     width="1366"
     id="rect"
     style="fill:url(#grad);fill-opacity:1;stroke:none;stroke-opacity:1" />
</svg>"""
    os.system("echo '{}' | inkscape -z -e /tmp/wall.png -w 1366 -h 768 - > /dev/null 2> /dev/null".format(svg.format(col1[0], col1[1], col1[2], col2[0], col2[1], col2[2])))
    os.system("feh --bg-fill /tmp/wall.png")
if __name__ == '__main__':
    while True:
        colors = getcolor(datetime.datetime.now())
        sbg(colors[0], colors[1])
        time.sleep(60)
