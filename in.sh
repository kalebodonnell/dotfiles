#!/bin/bash
echo "═══IN.SH═══"
echo "The arch configurer that doesn't work™"
echo "THIS SCRIPT ASSUMES YOU HAVE GONE THROUGH THE ARCH INSTALL GUIDE"
echo "DO NOT CONTINUE IF YOU HAVN'T"
read -p "Continue [Y/n]: " cont
if [[ $cont == 'n' ]]; then
	exit
fi
echo "═══IN═══"
read -p "username: " user
read -p "password: " pass
read -p "[IN] CONNECT TO THE INTERNET AND PRESS ENTER" d
echo "[IN] PASSING TO STAL"
./stal.sh $user $pass
echo "[IN] MAKING A COPY FOR LED TO WORK WITH"
mkdir /home/$user/dotfiles
cp -r * /home/$user/dotfiles
chown -R $user /home/$user/dotfiles
chgrp -R $user /home/$user/dotfiles
echo "[IN] PASSING TO LED AS $user"
su $user -c "cd ~/dotfiles; ./led.sh"
echo "[IN] SETTING $user shell"

echo "═══END IN═══"
